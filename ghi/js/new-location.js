window.addEventListener('DOMContentLoaded', async () => {

    // State fetching code, here...

    // Load the available locations and populate the location select tag
    const locationSelect = document.getElementById('location');
    axios.get('/api/locations')
    .then(response => {
        const locations = response.data.locations;
        locations.forEach(location => {
        const option = document.createElement('option');
        option.value = location.id;
        option.text = location.name;
        locationSelect.appendChild(option);
        });
    })
    .catch(error => {
        console.error(error);
        alert('An error occurred while loading the locations. Please try again later.');
    });

    // Handle form submission
    const form = document.getElementById('new-conference-form');
    form.addEventListener('submit', event => {
    event.preventDefault();

    const data = {
        name: form.elements['name'].value,
        starts: form.elements['start-date'].value,
        ends: form.elements['end-date'].value,
        max_presentations: form,

    }});
  });
